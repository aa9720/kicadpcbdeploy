
## Installation

Clone this repo to your kicad plugins folder first.

To set this up for best performance you'll need to make a new file called contactInfo.py in the same folder as this repo.
This file should look like this:
```python
contact = 'NAME EMAIL PHONE#'
```
You don't need to have all of that but what you do have should be one single string.
