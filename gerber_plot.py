import pcbnew
import os
import shutil
import subprocess
from .kicadFabNotes import generateFabNotes
import datetime
import sys
from .render_pcb import render_pcb
import threading

# SETTINGS:
# Gerber

# Drill
METRIC = True
ZERO_FORMAT = pcbnew.GENDRILL_WRITER_BASE.DECIMAL_FORMAT
INTEGER_DIGITS = 3
MANTISSA_DIGITS = 3
MIRROR_Y_AXIS = False
HEADER = True
OFFSET = pcbnew.wxPoint(0,0)
MERGE_PTH_NPTH = True
DRILL_FILE = True
MAP_FILE = False
REPORTER = None

def generate_gerbers(pcb, path):
    plot_controller = pcbnew.PLOT_CONTROLLER(pcb)
    plot_options = plot_controller.GetPlotOptions()

    # Set General Options:
    plot_options.SetOutputDirectory(path)
    plot_options.SetPlotFrameRef(False)
    plot_options.SetPlotValue(True)
    plot_options.SetPlotReference(True)
    plot_options.SetPlotInvisibleText(True)
    plot_options.SetPlotViaOnMaskLayer(True)
    plot_options.SetExcludeEdgeLayer(False)
    #plot_options.SetPlotPadsOnSilkLayer(PLOT_PADS_ON_SILK_LAYER)
    #plot_options.SetUseAuxOrigin(PLOT_USE_AUX_ORIGIN)
    plot_options.SetMirror(False)
    #plot_options.SetNegative(PLOT_NEGATIVE)
    #plot_options.SetDrillMarksType(PLOT_DRILL_MARKS_TYPE)
    #plot_options.SetScale(PLOT_SCALE)
    plot_options.SetAutoScale(True)
    #plot_options.SetPlotMode(PLOT_MODE)
    #plot_options.SetLineWidth(pcbnew.FromMM(PLOT_LINE_WIDTH))

    # Set Gerber Options
    #plot_options.SetUseGerberAttributes(GERBER_USE_GERBER_ATTRIBUTES)
    #plot_options.SetUseGerberProtelExtensions(GERBER_USE_GERBER_PROTEL_EXTENSIONS)
    #plot_options.SetCreateGerberJobFile(GERBER_CREATE_GERBER_JOB_FILE)
    #plot_options.SetSubtractMaskFromSilk(GERBER_SUBTRACT_MASK_FROM_SILK)
    #plot_options.SetIncludeGerberNetlistInfo(GERBER_INCLUDE_GERBER_NETLIST_INFO)

    plot_plan = [
        ( 'F.Cu', pcbnew.F_Cu, 'Front Copper' ),
        ( 'B.Cu', pcbnew.B_Cu, 'Back Copper' ),
        ( 'F.Paste', pcbnew.F_Paste, 'Front Paste' ),
        ( 'B.Paste', pcbnew.B_Paste, 'Back Paste' ),
        ( 'F.SilkS', pcbnew.F_SilkS, 'Front SilkScreen' ),
        ( 'B.SilkS', pcbnew.B_SilkS, 'Back SilkScreen' ),
        ( 'F.Mask', pcbnew.F_Mask, 'Front Mask' ),
        ( 'B.Mask', pcbnew.B_Mask, 'Back Mask' ),
        ( 'Edge.Cuts', pcbnew.Edge_Cuts, 'Edges' ),
        ( 'Eco1.User', pcbnew.Eco1_User, 'Eco1 User' ),
        ( 'Eco2.User', pcbnew.Eco2_User, 'Eco1 User' ),
    ]
    if pcb.GetCopperLayerCount() == 4:
        plot_plan.extend([
            ( "In1_Cu", pcbnew.In1_Cu, 'Inner1' ),
            ( "In2_Cu", pcbnew.In2_Cu, 'Inner2' )])

    for layer_info in plot_plan:
        plot_controller.SetLayer(layer_info[1])
        plot_controller.OpenPlotfile(layer_info[0], pcbnew.PLOT_FORMAT_GERBER, layer_info[2])
        plot_controller.PlotLayer()

    plot_controller.ClosePlot()


def detect_blind_buried_or_micro_vias(pcb):
    through_vias = 0
    micro_vias = 0
    blind_or_buried_vias = 0

    for track in pcb.GetTracks():
        if track.Type() != pcbnew.PCB_VIA_T:
            continue

        if track.GetShape() == pcbnew.VIA_THROUGH:
            through_vias += 1
        elif track.GetShape() == pcbnew.VIA_MICROVIA:
            micro_vias += 1
        elif track.GetShape() == pcbnew.VIA_BLIND_BURIED:
            blind_or_buried_vias += 1

    if micro_vias or blind_or_buried_vias:
        return True
    else:
        return False


def generate_drill_file(pcb, path):

    #if detect_blind_buried_or_micro_vias(pcb):
    #    return

    drill_writer = pcbnew.EXCELLON_WRITER(pcb)
    drill_writer.SetFormat(METRIC, ZERO_FORMAT, INTEGER_DIGITS, MANTISSA_DIGITS)
    drill_writer.SetOptions(MIRROR_Y_AXIS, HEADER, OFFSET, MERGE_PTH_NPTH)
    drill_writer.CreateDrillandMapFilesSet(path, DRILL_FILE, MAP_FILE, REPORTER)


def generate_POS(my_board, dirc):
    import os
    mm_ius = 1000000.0

    # my_board = pcbnew.GetBoard()

    fileName = my_board.GetFileName()

    dirpath = os.path.abspath(os.path.expanduser(fileName))
    path, fname = os.path.split(dirpath)
    ext = os.path.splitext(os.path.basename(fileName))[1]
    name = os.path.splitext(os.path.basename(fileName))[0]
    #wx.LogMessage(dir)
    #lsep=os.linesep
    lsep='\n'

    if len(dirc)>0:
        dirc = dirc.rstrip('\\').rstrip('/')
        if not os.path.exists(path+os.sep+dirc):
            #create dir
            os.mkdir(path+os.sep+dirc)
        dirc = dirc+os.sep
        #wx.LogMessage(dir)
    else:
        dirc = dirc+os.sep
    #LogMsg1=lsep+"reading from:" + lsep + dirpath + lsep + lsep
    out_filename_top_SMD=path+os.sep+dirc+name+"_POS_top_SMD.txt"
    out_filename_bot_SMD=path+os.sep+dirc+name+"_POS_bot_SMD.txt"
    out_filename_top_THD=path+os.sep+dirc+name+"_POS_top_THD.txt"
    out_filename_bot_THD=path+os.sep+dirc+name+"_POS_bot_THD.txt"
    out_filename_top_VIRTUAL=path+os.sep+dirc+name+"_POS_top_Virtual.txt"
    out_filename_bot_VIRTUAL=path+os.sep+dirc+name+"_POS_bot_Virtual.txt"
    out_filename_ALL=path+os.sep+dirc+name+"_POS_All.txt"

    Header_1="### Module positions - created on " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+lsep
    Header_1+="### Printed by pcb_positions plugin"+lsep
    Header_1+="## Unit = mm, Angle = deg."+lsep
    #LogMsg+="## Side : All"+lsep
    Header_2="## Board Aux Origin: " + str(my_board.GetAuxOrigin())+lsep

    Header_2+="{0:<10}".format("# Ref")+"{0:<20}".format("Val")+"{0:<20}".format("Package")+\
            "{0:<11}".format("PosX")+"{0:<11}".format("PosY")+"{0:<8}".format("  Rot")+\
            "{0:<10}".format("  Side")+"  Type"+lsep
    content_top_SMD=''
    content_bot_SMD=''
    content_top_THD=''
    content_bot_THD=''
    content_top_VIRTUAL=''
    content_bot_VIRTUAL=''
    content_ALL=''
    SMD_pads = 0
    TH_pads = 0
    Virt_pads = 0
    TH_top_cnt = 0
    TH_bot_cnt = 0
    SMD_top_cnt = 0
    SMD_bot_cnt = 0
    Virt_top_cnt = 0
    Virt_bot_cnt = 0

    bb = my_board.GetBoardEdgesBoundingBox()
    pcb_height = bb.GetHeight() / mm_ius
    pcb_width = bb.GetWidth() / mm_ius
    #to add relative position to
    #print ("Board Aux Origin: " + str(my_board.GetAuxOrigin()))

    #'{0:<10} {1:<10} {2:<10}'.format(1.0, 2.2, 4.4))

    tracks = my_board.GetTracks()
    vias = []
    for via in tracks:
        if type(via) is pcbnew.VIA:
            vias.append(via)
    vias_cnt = len(vias)

    for module in my_board.GetModules():
        #print ("%s \"%s\" %s %1.3f %1.3f %1.3f %s" % ( module.GetReference(),
        #Nchars = 20
        # RefL = 10; ValL = 20

        md=""
        if module.GetAttributes() == 0:   # PTH=0, SMD=1, Virtual = 2
            md = "THD"
            TH_pads+=module.GetPadCount()
        elif module.GetAttributes() == 1:
            md = "SMD"
            SMD_pads+=module.GetPadCount()
        else:
            md = "VIRTUAL"
            Virt_pads+=module.GetPadCount()

        #Reference=str(module.GetReference()).ljust(Nchars/2)
        #Value=str(module.GetValue()).ljust(Nchars)
        #Package=str(module.GetFPID().GetLibItemName()).ljust(Nchars)
        #X_POS=str(pcbnew.ToMM(module.GetPosition().x - my_board.GetAuxOrigin().x ))
        #Y_POS=str(-1*pcbnew.ToMM(module.GetPosition().y - my_board.GetAuxOrigin().y))
        #Rotation=str(module.GetOrientation()/10)
        #Layer="top".ljust(Nchars) if module.GetLayer() == 0 else "bottom".ljust(Nchars)
        #Type=md
        Reference="{0:<10}".format(str(module.GetReference()))
        Value = str(module.GetValue())
        Value=(Value[:17] + '..') if len(Value) > 19 else Value
        Value="{0:<20}".format(Value)
        Package = str(module.GetFPID().GetLibItemName())
        Package=(Package[:17] + '..') if len(Package) > 19 else Package
        Package="{0:<20}".format(Package)
        #Package="{0:<20}".format(str(module.GetFPID().GetLibItemName()))
        X_POS='{0:.4f}'.format(pcbnew.ToMM(module.GetPosition().x - my_board.GetAuxOrigin().x ))
        X_POS="{0:<11}".format(X_POS)
        Y_POS='{0:.4f}'.format(-1*pcbnew.ToMM(module.GetPosition().y - my_board.GetAuxOrigin().y))
        Y_POS="{0:<11}".format(Y_POS)
        Rotation='{0:.1f}'.format((module.GetOrientation()/10))
        Rotation="{0:>6}".format(Rotation)+'  '
        if module.GetLayer() == 0:
            Layer="  top"
        else:
            Layer="  bottom"
        #Side="## Side :"+Layer+lsep
        Layer="{0:<10}".format(Layer)
        Type='  '+md
        content=Reference
        content+=Value
        content+=Package
        content+=X_POS
        content+=Y_POS
        content+=Rotation
        content+=Layer
        content+=Type+lsep
        if 'top' in Layer and 'SMD' in Type:
            content_top_SMD+=content
            SMD_top_cnt+=1
        elif 'bot' in Layer and 'SMD' in Type:
            content_bot_SMD+=content
            SMD_bot_cnt+=1
        elif 'top' in Layer and 'THD' in Type:
            content_top_THD+=content
            TH_top_cnt+=1
        elif 'bot' in Layer and 'THD' in Type:
            content_bot_THD+=content
            TH_bot_cnt+=1
        elif 'top' in Layer and 'VIRTUAL' in Type:
            content_top_VIRTUAL+=content
            Virt_top_cnt+=1
        elif 'bot' in Layer and 'VIRTUAL' in Type:
            content_bot_VIRTUAL+=content
            Virt_bot_cnt+=1
        content_ALL+=content
        #print ("%s %s %s %1.4f %1.4f %1.4f %s %s" % ( str(module.GetReference()).ljust(Nchars),
        #                            str(module.GetValue()).ljust(Nchars),
        #                            str(module.GetFPID().GetLibItemName()).ljust(Nchars),
        #                            pcbnew.ToMM(module.GetPosition().x - my_board.GetAuxOrigin().x ),
        #                            -1*pcbnew.ToMM(module.GetPosition().y - my_board.GetAuxOrigin().y),
        #                            module.GetOrientation()/10,
        #                            "top".ljust(Nchars) if module.GetLayer() == 0 else "bottom".ljust(Nchars),
        #                            md
        #                            ))

#"top" if module.GetLayer() == 0 else "bottom"

    #LogMsg+="## End"+lsep
    Header=Header_1+"## Side : top"+lsep+Header_2
    content=Header+content_top_SMD+"## End"+lsep
    with open(out_filename_top_SMD,'w') as f_out:
        f_out.write(content)
    Header=Header_1+"## Side : bottom"+lsep+Header_2
    content=Header+content_bot_SMD+"## End"+lsep
    with open(out_filename_bot_SMD,'w') as f_out:
        f_out.write(content)
    Header=Header_1+"## Side : top"+lsep+Header_2
    content=Header+content_top_THD+"## End"+lsep
    with open(out_filename_top_THD,'w') as f_out:
        f_out.write(content)
    Header=Header_1+"## Side : bottom"+lsep+Header_2
    content=Header+content_bot_THD+"## End"+lsep
    with open(out_filename_bot_THD,'w') as f_out:
        f_out.write(content)
    Header=Header_1+"## Side : top"+lsep+Header_2
    content=Header+content_top_VIRTUAL+"## End"+lsep
    with open(out_filename_top_VIRTUAL,'w') as f_out:
        f_out.write(content)
    Header=Header_1+"## Side : bottom"+lsep+Header_2
    content=Header+content_bot_VIRTUAL+"## End"+lsep
    with open(out_filename_bot_VIRTUAL,'w') as f_out:
        f_out.write(content)
    Header=Header_1+"## Side : ALL"+lsep+Header_2
    content=Header+content_ALL+"## End"+lsep
    content = content + '## '+ str(SMD_pads) + ' SMD pads' +lsep
    content = content + '## '+ str(TH_pads) + ' TH pads' +lsep
    content = content + '## '+ str(Virt_pads) + ' Virtual pads' +lsep
    content = content + '## '+ str( TH_top_cnt) + ' Top TH modules' + lsep
    content = content + '## '+ str( TH_bot_cnt) + ' Bot TH modules' + lsep
    content = content + '## '+ str( SMD_top_cnt) + ' Top SMD modules' + lsep
    content = content + '## '+ str( SMD_bot_cnt) + ' Bot SMD modules' + lsep
    content = content + '## '+ str( Virt_top_cnt) + ' Top Virtual modules' + lsep
    content = content + '## '+ str( Virt_bot_cnt) + ' Bot Virtual modules' + lsep

    with open(out_filename_ALL,'w') as f_out:
        f_out.write(content)

    #with open(out_filename,'w') as f_out:
    #    f_out.write(LogMsg)
    #LogMsg=""
    #f = open(out_filename,'w')
    #f.write(LogMsg)
    #f.close()
    LogMsg1="reading from:" + lsep + dirpath + lsep
    LogMsg1+= lsep + 'Pads:' + lsep
    LogMsg1+= 'SMD pads ' + str(SMD_pads) + lsep
    LogMsg1+= 'TH pads ' + str(TH_pads) +lsep
    LogMsg1+= 'Virtual pads ' + str(Virt_pads) + lsep
    LogMsg1+= 'Vias ' + str( vias_cnt) + lsep
    LogMsg1+= lsep + 'Modules:' + lsep
    LogMsg1+= 'Top TH modules ' + str( TH_top_cnt) + lsep
    LogMsg1+= 'Bot TH modules ' + str( TH_bot_cnt) + lsep
    LogMsg1+= 'Top SMD modules ' + str( SMD_top_cnt) + lsep
    LogMsg1+= 'Bot SMD modules ' + str( SMD_bot_cnt) + lsep
    LogMsg1+= 'Top Virtual modules ' + str( Virt_top_cnt) + lsep
    LogMsg1+= 'Bot Virtual modules ' + str( Virt_bot_cnt) + lsep
    LogMsg1+= lsep + 'PCB Geometry:' + lsep
    LogMsg1+= 'Pcb Height ' +'{0:.3f}'.format( pcb_height ) + 'mm, Pcb Width ' + '{0:.3f}'.format( pcb_width ) + 'mm' +lsep+'[based on Edge bounding box]' +lsep
    LogMsg1+= lsep
    #LogMsg1+=lsep+"reading from:" + lsep + dirpath + lsep + lsep
    if 0:
        LogMsg1+="written to:" + lsep + out_filename_top_SMD + lsep
        LogMsg1+=out_filename_bot_SMD + lsep
        LogMsg1+=out_filename_top_THD + lsep
        LogMsg1+=out_filename_bot_THD + lsep
        LogMsg1+=out_filename_top_VIRTUAL + lsep
        LogMsg1+=out_filename_bot_VIRTUAL + lsep
        LogMsg1+=out_filename_ALL + lsep
    else:
        LogMsg1+="written to:" + lsep + path+os.sep+dirc + lsep

    return LogMsg1


class SimplePlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = 'Gerber Plot'
        self.category = 'Gerber'
        self.description = 'Generate Gerber files, drill holes, see the result and send to a compressed folder'
        self.show_toolbar_button = True
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'gerber_plot_icon.png')

    def Run(self):
        # The entry function of the plugin that is executed on user action
        try:
            cwd_path = os.getcwd()
            pcb = pcbnew.GetBoard()
            project_path, project_name = os.path.split(pcb.GetFileName())
            project_name = os.path.splitext(project_name)[0]
            output_path = os.path.join(project_path, project_name + '-Gerber').replace('\\','/')
            tmp_path = os.path.join(project_path, 'tmp').replace('\\','/')
            log_file = os.path.join(project_path, 'log.txt').replace('\\','/')
            if os.path.exists(log_file):
                os.remove(log_file)
        except Exception as err:
            with open(log_file, 'a') as file:
                file.write('Startup error\nError:{}\n'.format(err))

        # Render an image: we need to call an external script that uses python 3

        # Create a temp folder
        try:
            if not os.path.exists(tmp_path):
                os.mkdir(tmp_path)
        except Exception as err:
            with open(log_file, 'a') as file:
                file.write('tmp folder not created\nError:{}\n'.format(err))

        # Generate Gerber and drill files
        try:
            generate_gerbers(pcb, tmp_path)
        except Exception as err:
            with open(log_file, 'a') as file:
                file.write('Gerbers not plotted\nError:{}\n'.format(err))

        try:
            generate_drill_file(pcb, tmp_path)
            generate_POS(pcb, 'tmp')
        except Exception as err:
            with open(log_file, 'a') as file:
                file.write('Drill file not plotted\nError:{}\n'.format(err))
        try:
            th = threading.Thread(target=render_pcb, args=[tmp_path])
            th.start()
        except Exception as err:
            with open(log_file, 'a') as file:
                file.write('PCB not rendered\nError:{}\n'.format(err))
        try:
            mdFile = generateFabNotes(pcb, tmp_path)
        except Exception as err:
            with open(log_file, 'a') as file:
                file.write('could not generate fab notes\nError:{}\n'.format(err))
        subprocess.call(['pandoc','%s' % mdFile,'-f','gfm','-V','linkcolor:blue','--pdf-engine=xelatex','-V','pagestyle=empty','-o','%s' % (mdFile.replace('.md', '.pdf'))])
        # Create compressed file from tmp
        try:
            os.chdir(tmp_path)
            shutil.make_archive(output_path, 'zip', tmp_path)
            os.chdir(cwd_path)
        except Exception as err:
            with open(log_file, 'a') as file:
                file.write('ZIP file not created\nError:{}\n'.format(err))

        # Remove temp folder
        try:
            th.join()
            shutil.rmtree(tmp_path, ignore_errors=True)
        except Exception as err:
            with open(log_file, 'a') as file:
                file.write('temp folder not deleted\nError:{}\n'.format(err))