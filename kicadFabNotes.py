#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 06:41:48 2020

@author: allard
"""

import pcbnew
import svgutils
import os
import re
import pypandoc

try:
    from contactInfo import contact
except:
    contact = 'YOUR NAME CONTACT INFO HERE'


def generateFabNotes(bd, fabPath):
    path, file = os.path.split(bd.GetFileName())
    height = bd.GetBoardEdgesBoundingBox().GetHeight()/1e6
    width = bd.GetBoardEdgesBoundingBox().GetWidth()/1e6
    layers = bd.GetCopperLayerCount()


    minWidth = 1
    minClearence = 1
    hasBlindOrBuriedVias = False
    minDrillSize = 1

    for t in bd.GetTracks():
        if isinstance(t, pcbnew.VIA):
            if t.GetViaType() == pcbnew.VIA_BLIND_BURIED or t.GetViaType() == pcbnew.VIA_MICROVIA:
                hasBlindOrBuriedVias = True
            if t.GetDrill() > 0:
                minDrillSize = min(minDrillSize, t.GetDrill()/1e6)

        elif t.IsTrack():
            minWidth = min(minWidth, t.GetWidth()/1e6)
        minClearence = min(minClearence, t.GetClearance()/1e6)


    f = open(fabPath + '/' + file.replace('.kicad_pcb', 'FabNotes.md'), 'w')
    f.write('# Fabrication notes\n')
    f.write('1. PCB is not RoHS\n')
    f.write('2. PCB dimensions are in mm unless otherwise specified.\n\t1. Dimensions at extrema: %.1fmm by %.1fmm\n' % (width, height))
    f.write('3. Copper:\n\t1. Layers: %g\n\t2. Min Trace Width %.2fmm\n\t3. Min Trace Separation: %.2fmm\n' %(layers, minWidth, minClearence))
    f.write('5. Solder mask\n\t1. Top and Bottom required\n')
    f.write('5. Silkscreen\n\t1. Top and Bottom required\n\t2. Silkscreen shall be white\n')
    f.write('6. Drill holes\n\t')
    if hasBlindOrBuriedVias:
        f.write('1. Has blind and buried vias\n')
    else:
        f.write('1. Has NO blind and buried vias\n')
    f.write('7. Contact information\n\t1. {0}\n\n'.format(contact))


    #%%

    plotDir = fabPath + '/plots'


    if not os.path.exists(plotDir):
        os.mkdir(plotDir)
    if layers > 2:
        layers = [
            ("F_SilkS", pcbnew.F_SilkS, "Silk top", 'FFFFFF'),
            ("F_Cu", pcbnew.F_Cu, "Top layer", 'FF0000'),
            ("In1_Cu", pcbnew.In1_Cu, "Inner1", '808000'),
            ("Edge_Cuts", pcbnew.Edge_Cuts, "Edges", '809000'),
            ("In2_Cu", pcbnew.In2_Cu, "Inner2", '800080'),
            ("B_Cu", pcbnew.B_Cu, "Bottom layer", '00C000'),
            ("B_SilkS", pcbnew.B_SilkS, "Silk top", 'FFFFFF')
        ]
    else:
        layers = [
            ("F_SilkS", pcbnew.F_SilkS, "Silk top", 'FFFFFF'),
            ("F_Cu", pcbnew.F_Cu, "Top layer", 'FF0000'),
            ("Edge_Cuts", pcbnew.Edge_Cuts, "Edges", '809000'),
            ("B_Cu", pcbnew.B_Cu, "Bottom layer", '00C000'),
            ("B_SilkS", pcbnew.B_SilkS, "Silk top", 'FFFFFF')
        ]

    pctl = pcbnew.PLOT_CONTROLLER(bd)
    pctl.SetColorMode(False)
    popt = pctl.GetPlotOptions()
    popt.SetOutputDirectory(plotDir)
    popt.SetDrillMarksType(1)
    popt.SetPlotFrameRef(False)
    popt.SetLineWidth(pcbnew.FromMM(0.15))
    popt.SetAutoScale(False)
    popt.SetScale(2)
    popt.SetMirror(False)
    popt.SetUseGerberAttributes(True)
    popt.SetExcludeEdgeLayer(True)
    popt.SetUseAuxOrigin(True)
    # c = pcbnew.COLOR4D(1, 1, 1, 0)
    # popt.SetColor(c)
    for l in layers:
        pctl.SetLayer(l[1])
        pctl.OpenPlotfile(l[0], pcbnew.PLOT_FORMAT_SVG, l[2])
        pctl.PlotLayer()
    del pctl



    for l in layers[::-1]:
        svgFile = plotDir + '/' + file.replace('.kicad_pcb', '') + '-'+l[0]+'.svg'
        svg = svgutils.transform.fromfile(svgFile)
        svg = svg.to_str().decode().replace('stroke:#000000;', 'stroke:#%s;' % l[3]).replace('fill:#000000;', 'fill:#%s;' % l[3])
        # svg.save(svgFile)
        if not 'layout' in vars().keys():
            layout = svgutils.transform.fromstring(svg)
        else:
            layout.append(svgutils.transform.fromstring(svg))
        os.remove(svgFile)

    layout.save(plotDir + '/' + file.replace('.kicad_pcb', '') + '.svg')

    svg = svgutils.transform.fromfile(plotDir + '/' + file.replace('.kicad_pcb', '') + '.svg')
    svgString = svg.to_str().decode()
    theLine = re.findall(r'(<svg.+">)', svgString)[1]
    svgString = svgString.replace(theLine, '').replace('</svg:svg>', '')

    svgutils.transform.fromstring(svgString).save(plotDir + '/' + file.replace('.kicad_pcb', '.svg'))

    f.write('![](./plots/{0})\n\n'.format(file.replace('.kicad_pcb', '') + '.svg', fabPath))

    # This gets generated later.....
    f.write('![](./pcb.jpg)'.format(fabPath))

    f.close()
    return fabPath + '/' + file.replace('.kicad_pcb', 'FabNotes.md')
    # return svgString

